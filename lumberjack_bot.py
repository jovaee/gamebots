import pyautogui
import time


_green = (126, 173, 79)

_pxl_left = (875, 608)
_pxl_right = (1045, 608)


def play():
    
    pyautogui.FAILSAFE = True  # Stops the program when mouse if moved to upper-left corner of screen
    pyautogui.PAUSE = 0
    
    while True:
        
        #img = pyautogui.screenshot(region=(825, 600, 350, 20))
        img = pyautogui.screenshot()
        
        pxl_left = img.getpixel(_pxl_left)
        pxl_right = img.getpixel(_pxl_right)
        
        #if not (pxl_left == _green or pxl_right == _green):
        #    print('[EVENT] Nothing found, chopping right')
            
        #    pyautogui.keyDown('right')
        #    pyautogui.keyUp('right')

       # else:
        if pxl_right == _green:
            print('[EVENT] Chopping left')

            pyautogui.keyDown('left')
            pyautogui.keyUp('left')

            time.sleep(0.10)

            pyautogui.keyDown('left')
            pyautogui.keyUp('left')

        else:
            print('[EVENT] Chopping right')
            
            pyautogui.keyDown('right')
            pyautogui.keyUp('right')
            
            time.sleep(0.10)

            pyautogui.keyDown('right')
            pyautogui.keyUp('right')
                
        time.sleep(0.100)


def main():
    time.sleep(5)  # Wait 5 seconds before actually begining
    
    play()


if __name__ == '__main__':
    main()